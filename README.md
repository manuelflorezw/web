# web



## Información del Repositorio
Este proyecto proporciona la misma visual construida en diferentes tecnologias a nivel de Front-end
siendo las ramas angular, vue, react. Respectivamente correspondiente a su tecnologia:
 - [angular](https://angular.io/)
 - [vue](https://vuejs.org/)
 - [react](https://reactjs.org/)
## Información de como construir las imagenes con docker
Construimos la imagen usando nuestro comando `docker build`

```
docker build -t manuelflorezw/web/[tecnología]:[versión] .
```

### Antes de construir la imagen de contenedor
los siguientes comandos se usan igual para las ramas `angular`, `vue` y `react`
```
git checkout [angular | vue | react]
npm install
npm run build
```
Los siguientes son ejemplos de como construir las imagenes de contenedores para las diferentes tecnologías implementadas:

 - angular
   ```
    docker build -t manuelflorezw/web-angular:v1.0.0 .
   ```
 - vue
   ```
    docker build -t manuelflorezw/web-vue:v1.0.0 .
   ```
 - react
   ```
    docker build -t manuelflorezw/web-react:v1.0.0 .
   ```
## Información de como ejecutar los contenedores docker
Los siguientes son ejemplos de como ejecutar los contenedores docker construidos anteriormente:

 - angular
   ```
    docker run --name web-angular -p 8080:80 -d manuelflorezw/web-angular:v1.0.0
   ```
 - vue
   ```
    docker run --name web-vue -p 8081:80 -d manuelflorezw/web-vue:v1.0.0
   ```
 - react
   ```
    docker run --name web-react -p 8082:80 -d manuelflorezw/web-react:v1.0.0
   ```

Si ejecutamos los contenedores en diferentes puertos obtenemos las tres aplicaciones ejecutandose en la misma maquina en los puertas `8081`, `8082`, `8083` correspondientes a `angular`, `vue` y `react` respectivamente.

## Backend 

Para conectar al backend tenemos varias opciones tecnológicas que encontraras en el siguiente repositorio [backend](https://gitlab.com/manuelflorezw/backend)

